Upstart
=======

Install
-------

```plain
cp etc/default/promexec /etc/default/promexec
cp etc/init/promexec.conf /etc/init/promexec.conf
```

Start
-----

```plain
initctl start promexec
```

Check log

```plain
tail -f /var/log/upstart/promexec.log
```
