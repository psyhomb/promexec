SystemD
=======

Create
------

Execute following `systemctl` command and copy the content from [promexec.service](./promexec.service) unit file

```plain
systemctl edit --full --force promexec.service
```

Copy `promexec` configuration file to its place

```plain
cp etc/default/promexec /etc/default/promexec
```

Start
-----

Enable and start `promexec` service

```plain
systemctl enable --now promexec.service
```

Check the status

```plain
systemctl status promexec.service
```
