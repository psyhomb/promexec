promexec
========

<div align="center">

![promexec_logo_final](/uploads/58d746e2b7de48464b83ec2c43b7b0f2/promexec_logo_final.png)
</div>

About
-----

Prometheus Executor (agent) will execute any whitelisted command on the localhost, passed to it by [promexec-router](https://gitlab.com/psyhomb/promexec-router).

Let's say for example an alert was received about disk usage and it is already known why disk is filling up and what has to be done for space to be reclaimed. All we have to do is create a script (any language), install it on the problematic nodes and `promexec` will do all the rest, every time alert is fired configured script will be executed on the designated nodes and alert will be automagically resolved.

Example request:

```json
{
  "alerts": [
    {
      "status": "firing",
      "labels": {
        "alertname": "disk_usage_critical",
        "correlate": "disk_usage_warning,disk_usage_critical",
        "device": "/dev/sda1",
        "event": "Disk Usage server1:/",
        "fstype": "ext4",
        "instance": "server1",
        "job": "node-exporter",
        "monitor": "prometheus",
        "mountpoint": "/",
        "node": "server1",
        "ipaddress": "10.0.0.1",
        "promexecCommand": "promexec-scriptname.sh",
        "promexecSnooze": "900",
        "promexecSnoozeResolved": "false",
        "service": "node",
        "severity": "critical",
        "timeout": "300"
      },
      "annotations": {
        "summary": "Disk usage for mountpoint server1:/ is at 91%",
        "value": "91%"
      },
      "startsAt": "2019-11-04T17:52:19.231805866Z",
      "endsAt": "0001-01-01T00:00:00Z",
      "generatorURL": "http://10.0.0.10:9090/graph?g0.expr=instance%3Adisk_usage%3Apercent+%3E%3D+90&g0.tab=1"
    }
  ]
}
```

Configuration
-------------

### promexec

| Env vars                 |  Env (default) Values  | Mandatory | Description                                          |
|:-------------------------|:-----------------------|:----------|:-----------------------------------------------------|
| `PROMEXEC_IPADDRESS`     | 0.0.0.0                | no        | Bind to IP address                                   |
| `PROMEXEC_PORT`          | 8080                   | no        | Bind to port                                         |
| `PROMEXEC_ALLOWED_BINS`  |                        | no        | Comma-separated list of allowed binaries (whitelist) |

`promexec` API has only one HTTP path `api/exec` and it supports only HTTP `POST` method.

```bash
curl -v -sSL -X POST -d @alerts.json http://localhost:8080/api/exec
```

### Prometheus

Supported labels (Prometheus alert rules):

| Lables                   | Value example            | Required  | Description                                                                                                         |
|:-------------------------|:-------------------------|:----------|:--------------------------------------------------------------------------------------------------------------------|
| `promexecCommand`        | promexec-scriptname.sh   | yes       | Script that's going to be executed on the local system where `promexec` is running                                  |
| `promexecSnooze`         | 900                      | yes       | Skip command execution for every subsequent alert with the same name and status for number of seconds               |
| `promexecSnoozeResolved` | false                    | no        | If true snooze will be removed when resolved status received, if false snooze has to expire                         |

In this example `promexecCommand` will be triggered only if disk usage goes over 90% (critical):

```yaml
groups:
  ### Disk Usage
  - name: disk_usage
    rules:
      - alert: disk_usage_warning
        expr: 80 < instance:disk_usage:percent < 90
        for: 0s
        labels:
          instance: '{{ $labels.node }}'
          service: node
          event: 'Disk Usage {{ $labels.node }}:{{ $labels.mountpoint }}'
          correlate: disk_usage_warning,disk_usage_critical
          severity: warning
        annotations:
          summary: 'Disk usage for mountpoint {{ $labels.node }}:{{ $labels.mountpoint }} is at {{ $value }}%'
          value: '{{ $value }}%'

      - alert: disk_usage_critical
        expr: instance:disk_usage:percent >= 90
        for: 0s
        labels:
          instance: '{{ $labels.node }}'
          service: node
          event: 'Disk Usage {{ $labels.node }}:{{ $labels.mountpoint }}'
          ### promexec-router labels
          ipaddress: '{{ $labels.ipaddress }}'
          promexecMatcherLabel: 'instance'
          promexecMatcherRegex: '^server[0-9]*'
          promexecMatcherRegexNot: '^(server5|server6)$'
          ### promexec labels
          promexecCommand: 'promexec-scriptname.sh'
          promexecSnooze: 900
          promexecSnoozeResolved: false
          ### END
          correlate: disk_usage_warning,disk_usage_critical
          severity: critical
        annotations:
          summary: 'Disk usage for mountpoint {{ $labels.node }}:{{ $labels.mountpoint }} is at {{ $value }}%'
          value: '{{ $value }}%'
```

Build
-----

Build `promexec` binary

```bash
make build_in_docker
```

Copy the binary file to destination host and create [systemd](./init/systemd) unit

```bash
scp promexec root@<destination_host_address>:/usr/local/bin/
```

Scripts
-------

In order to create non-blocking command/script I'd highly recommend starting the script within `screen`.

So for example if in `PROMEXEC_ALLOWED_BINS` list and as value of `promexecCommand` label you specify `promexec-scriptname.sh`, you should create two scripts:

`/usr/local/bin/promexec-entrypoint.sh`

```bash
#!/bin/bash

ARGS=${@}
BASENAME="$(basename ${0})"
SCRIPT_NAME="${BASENAME/promexec-/}"
SESSION_NAME="${BASENAME%.*}"

screen -c /dev/null -dmS ${SESSION_NAME} ${SCRIPT_NAME} ${ARGS}
```

Create symbolic link:

```plain
cd /usr/local/bin
ln -snf promexec-entrypoint.sh promexec-scriptname.sh
```

`/usr/local/bin/scriptname.sh`

```bash
#!/bin/bash

### Your code goes here ###
# START - Example code
ARGS=${@}
SCRIPT_NAME="$(basename ${0})"

if [[ -z ${ARGS} ]]; then
  ARGS="empty"
fi

echo "$(date) Executed: ${SCRIPT_NAME} ${ARGS}" >> /root/${SCRIPT_NAME%.*}.log
sleep 30
# END - Example code
```

In this example we're using bash to code `scriptname.sh` but you can use any programming language you like.
