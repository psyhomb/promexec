package main

import (
	"fmt"
	"log"
	"net/http"
	"os"

	"github.com/gin-gonic/gin"
	"gitlab.com/psyhomb/promexec/alert"
)

// Binary name and version
var (
	binName string
	version string
)

// Environment variables
var (
	ipAddress string
	port      string
	ginMode   string
)

// ReqJSONBody struct will be used to unmarshal JSON request body
type ReqJSONBody struct {
	Alerts []alert.Alert `json:"alerts"`
}

func init() {
	log.Printf("[INFO] Running %v rev %v", binName, version)

	port, _ = os.LookupEnv("PROMEXEC_PORT")
	if len(port) == 0 {
		port = "8080"
		log.Printf("[INFO] PROMEXEC_PORT environment variable not defined, falling back to default value %v", port)
	}

	ipAddress, _ = os.LookupEnv("PROMEXEC_IPADDRESS")
	if len(ipAddress) == 0 {
		ipAddress = "0.0.0.0"
		log.Printf("[INFO] PROMEXEC_IPADDRESS environment variable not defined, falling back to default value %v", ipAddress)
	}

	ginMode, _ = os.LookupEnv("GIN_MODE")
	if len(ginMode) == 0 {
		ginMode = "release"
	}
}

func main() {
	gin.SetMode(ginMode)
	r := gin.Default()

	execPath := "api/exec"
	r.POST(execPath, res)

	tcpIPSocket := fmt.Sprintf("%v:%v", ipAddress, port)
	r.Run(tcpIPSocket)
}

// Generate HTTP JSON response
func res(c *gin.Context) {
	var req ReqJSONBody

	// Unmarshal JSON (HTTP request) to req struct
	err := c.ShouldBindJSON(&req)
	if err != nil {
		log.Printf("[ERROR] %v", err)
		c.JSON(http.StatusBadRequest, gin.H{
			"message": err.Error(),
		})

		return
	}

	// Iterate through list of alerts and execute commands unless snoozed
	for _, a := range req.Alerts {
		a.Process()
	}

	// Accept request from caller
	c.JSON(http.StatusOK, gin.H{
		"message": "OK",
	})
}
