package alert

import (
	"log"
	"os"
	"os/exec"
	"strconv"
	"strings"
)

var (
	snoozeMemDB SnoozeMap
	allowedBins []string
)

// Alert struct holds collection of labels coming from alert rule definitions
type Alert struct {
	Labels       map[string]string `json:"labels"`
	Annotations  map[string]string `json:"annotations"`
	Status       string            `json:"status"`
	StartsAt     string            `json:"startsAt"`
	EndsAt       string            `json:"endsAt"`
	GeneratorURL string            `json:"generatorURL"`
}

func init() {
	v, _ := os.LookupEnv("PROMEXEC_ALLOWED_BINS")
	if len(v) == 0 {
		log.Printf("[INFO] PROMEXEC_ALLOWED_BINS environment variable not defined, falling back to default value %v", allowedBins)
	} else {
		for _, allowedBin := range strings.Split(v, ",") {
			allowedBins = append(allowedBins, strings.Trim(allowedBin, " "))
		}
	}
}

// Process all received alerts
func (a Alert) Process() {
	alertname := a.Labels["alertname"]
	command := a.Labels["promexecCommand"]

	// Check if required labels are present in alerts' labels list, if not skip
	requiredLabels := []string{"promexecCommand", "promexecSnooze"}
	if !isSliceInMapKeys(requiredLabels, a.Labels) {
		log.Printf(
			"[INFO] Skipping Alertname:%v, have not found required labels %q",
			alertname, requiredLabels,
		)

		return
	}

	// Delete snooze pair for alerts with status resolved if label promexecSnoozeResolved set to true
	if a.Status == "resolved" {
		snoozeResolved, _ := strconv.ParseBool(a.Labels["promexecSnoozeResolved"])
		if snoozeResolved {
			log.Printf(
				"[INFO] Received status resolved: deleting snooze pair for Alertname:%v Command:%v",
				alertname, command,
			)
			snoozeMemDB.Remove(alertname)

			return
		}

		log.Printf(
			"[INFO] Received status resolved: skipping Alertname:%v Command:%v",
			alertname, command,
		)

		return
	}

	// Continue execution if status firing

	// Command execution will proceed only if binary from command is in the allowed binaries list
	bin := strings.Split(command, " ")[0]
	if !isInSlice(bin, allowedBins) {
		log.Printf(
			"[INFO] Binary %v is not in the list of allowed binaries, please check PROMEXEC_ALLOWED_BINS environment variable",
			bin,
		)

		return
	}

	// Snooze command execution by adding snooze information
	snooze, err := strconv.Atoi(a.Labels["promexecSnooze"])
	if err != nil {
		log.Println("[ERROR] promexecSnooze label must have numeric value")

		return
	}

	elapsedTime, expired := snoozeMemDB.IsExpired(alertname, snooze)
	if expired {
		snoozeMemDB.Update(alertname)
	} else {
		log.Printf(
			"[INFO] Execution has been snoozed %d/%d seconds for Alertname:%v Command:%v",
			elapsedTime, snooze, alertname, command,
		)

		return
	}

	// Execute command
	// failed
	out, err := execCmd(command)
	if err != nil {
		log.Printf(
			"[ERROR] Execution of Alertname:%v Command:%v has failed due to error: %v: %s",
			alertname, command, err.Error(), err.(*exec.ExitError).Stderr,
		)

		return
	}

	// successed
	log.Printf(
		"[INFO] Successfully executed Alertname:%v Command:%v Output:%s",
		alertname, command, out,
	)
}

// ExecCmd will execute command and return output and error if any
func execCmd(c string) ([]byte, error) {
	s := strings.Split(c, " ")
	basename, args := s[0], s[1:]

	return exec.Command(basename, args...).Output()
}

// IsSliceInMapKeys checks if all elements from provided slice are present in the list of map keys
func isSliceInMapKeys(s []string, m map[string]string) bool {
	ns := make([]string, 0)

	for k := range m {
		ns = append(ns, k)
	}

	for _, v := range s {
		if !isInSlice(v, ns) {
			return false
		}
	}

	return true
}

// IsInSlice checks if provided string is in the slice
func isInSlice(v string, s []string) bool {
	for _, e := range s {
		if e == v {
			return true
		}
	}

	return false
}
