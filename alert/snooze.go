package alert

import (
	"crypto/md5"
	"fmt"
	"sync"
	"time"
)

// SnoozeMap creates threadsafe map that holds snooze pairs (hash:unixTime)
type SnoozeMap struct {
	sync.Map
}

// Update or create snooze pairs (hash:unixTime)
func (sm *SnoozeMap) Update(s string) *SnoozeMap {
	data := []byte(s)
	nowTime := int(time.Now().UTC().Unix())
	hash := fmt.Sprintf("%x", md5.Sum(data))

	sm.Store(hash, nowTime)

	return sm
}

// Cleanup outdated snooze pairs if older than expirationTime in seconds
func (sm *SnoozeMap) Cleanup(expirationTime int) {
	sm.Range(func(h interface{}, rt interface{}) bool {
		hash := h.(string)
		recordedTime := rt.(int)
		nowTime := int(time.Now().UTC().Unix())

		if nowTime-recordedTime >= expirationTime {
			sm.Delete(hash)
		}

		return true
	})
}

// Remove snooze pair (hash:unixTime)
func (sm *SnoozeMap) Remove(s string) {
	data := []byte(s)
	hash := fmt.Sprintf("%x", md5.Sum(data))

	sm.Delete(hash)
}

// IsExpired checks if elapsed time is greater than expiration time and if true consider snooze pair expired
func (sm *SnoozeMap) IsExpired(s string, expirationTime int) (int, bool) {
	data := []byte(s)
	hash := fmt.Sprintf("%x", md5.Sum(data))
	recordedTime := 0

	rt, ok := sm.Load(hash)
	if ok {
		recordedTime = rt.(int)
	}

	nowTime := int(time.Now().UTC().Unix())
	elapsedTime := nowTime - recordedTime

	switch {
	case recordedTime == 0:
		return 0, true
	case elapsedTime > expirationTime:
		return expirationTime, true
	}

	return elapsedTime, false
}
